import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'dart:math';

void main() {
  runApp(LogoApp());
}
class LogoApp extends StatefulWidget {
  @override
  _LogoAppState createState() => _LogoAppState();
}
class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  Animation<Color> color;
  AnimationController controller;


  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    color = ColorTween(begin: Colors.yellowAccent[700], end: Colors.yellowAccent[100]).animate(
        controller);
    animation = Tween<double>(begin: 200, end: 320).animate(controller)
      ..addStatusListener((status) {
        if(status == AnimationStatus.completed){
          controller.reverse();
        } else if (status == AnimationStatus.dismissed){
          controller.forward();
        }
      })
      ..addStatusListener((status) => print('$status'));
    controller.forward();
  }

    @override
    void dispose(){
      controller.dispose();
      super.dispose();
    }
    @override
    Widget build(BuildContext context) {
      return MaterialApp(
        title: 'Animation App',
        theme: ThemeData(
          scaffoldBackgroundColor: Colors.black,
        ),
        home: Scaffold(
          appBar: AppBar(title: Text('Animation App')),
          body: Padding(
            padding: const EdgeInsets.all(24),
            child: Column(
              children: [
                AnimatedBuilder(
                  animation: color,
                  builder: (BuildContext context, _) {
                    return Transform.rotate(
                      angle: controller.value * 2 * pi,
                      child: Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        height: animation.value,
                        width: animation.value,
                        child: ClipPath(
                          clipper: StarClipper(5),
                          child: Container(
                            color: color.value,
                          ),
                        ),
                      ),
                    );
                    },
                ),
                Text('LOADING....', style: TextStyle(
                    fontSize: 24,
                    color: Colors.white)
                ),
                AnimatedBuilder(
                   animation: color,
                   builder: (BuildContext context, _) {
                     return Slider(
                       value: controller.value,
                       onChanged: (value) {
                         setState(() {
                           controller.value = value;
                         });
                       },
                     );
                   },
                ),
              ],
            ),
          ),
        ),
      );
  }
}


